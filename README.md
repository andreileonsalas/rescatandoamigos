# Quasar App (rescatandoamigos)

Aplicacion utilizando el framework Quasar

Primero ocupamos instalar nodejs y yarn.
Si esta en windows, puedes utilizar estos comandos desde powershell con permisos de adminsitrador (en caso de dudas referirse a https://chocolatey.org/install)
```bash
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco install nodejs yarn
```

Una vez instalado yarn y nodejs, agregamos a quasar/cli al sistema
yarn global add @quasar/cli


## Instalacion de dependencia
```bash
yarn
```

### Iniciar la app en modo desarrollador
```bash
quasar dev
```

### Construir la aplicacion para produccion y compatible con celular
```bash
quasar build -m pwa
```

### Comando para agregar gravatar
yarn add vue-gravatar

### Pasos para hacer deploy a produccion

Desde la computadora nuestra, utilizamos quasar build -m pwa  
Copiamos el contenido de la carpeta dist/spa a nuestro servidor apache  
Una vez podamos acceder al sitio web, copiamos la url de produccion  
Ingresamos a https://www.pwabuilder.com/ y seguimos los pasos  
Bajamos el apk y lo distribuimos manualmente (Si se paga por ser developer en android, podemos distribuirlo por esa plataforma)