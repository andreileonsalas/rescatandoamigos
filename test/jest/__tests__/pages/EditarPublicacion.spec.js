import { mount, createLocalVue, shallowMount } from '@vue/test-utils';
import EditarPublicacion from '../../../../src/pages/EditarPublicacion.vue';
import * as All from 'quasar';
import firebase from "firebase";
const firestoreMock = { 
  collection: jest.fn().mockReturnThis(), 
  doc: jest.fn().mockReturnThis(), 
  set: jest.fn().mockResolvedValueOnce(), 
};  
//import { dba } from "../../../../";



// import langEn from 'quasar/lang/en-us' // change to any language you wish! => this breaks wallaby :(
const { Quasar } = All;

describe('Functional Suitability-Functional Correctness: RF011-Modificar de publicación', () => {

  beforeAll(function() { 
    firebase.auth = jest.fn().mockReturnValue({ 
    currentUser: { email: 'example@gmail.com', uid: 1, emailVerified: true }, 
    signOut: function() { return true; } 
    }); 
  });

  it('should return user', () => {
    firebase.auth.mockReturnValueOnce({
      currentUser: { email: 'example@gmail.com', uid: 1, emailVerified: true }, 
    });
    const components = Object.keys(All).reduce((object, key) => {
      const val = All[key];
      if (val && val.component && val.component.name != null) {
        object[key] = val;
      }
      return object;
    }, {});
    const localVue = createLocalVue();
      localVue.use(Quasar, { components }); // , lang: langEn
      const wrapper = mount(EditarPublicacion, {
        localVue,
      });

    const vm= wrapper.vm;
    expect(vm.Correo).toEqual('example@gmail.com');
  });


  it('Email Correcto', () => {

    firebase.auth.mockReturnValueOnce({
      currentUser: { email: 'example@gmail.com', uid: 1, emailVerified: true }, 
    });
    const components = Object.keys(All).reduce((object, key) => {
      const val = All[key];
      if (val && val.component && val.component.name != null) {
        object[key] = val;
      }
      return object;
    }, {});
    const localVue = createLocalVue();
      localVue.use(Quasar, { components }); // , lang: langEn
      const wrapper = mount(EditarPublicacion, {
        localVue,
      });

      const vm= wrapper.vm;

    expect(vm.validateEmail("correo@mail.com")).toBe(true);

  });

  // eslint-disable-next-line jest/no-commented-out-tests
  // it('Correo empieza con punto', () => {

    
  //   firebase.auth.mockReturnValueOnce({
  //     currentUser: { email: '.correo@mail.com', uid: 1, emailVerified: true }, 
  //   });
  //   const components = Object.keys(All).reduce((object, key) => {
  //     const val = All[key];
  //     if (val && val.component && val.component.name != null) {
  //       object[key] = val;
  //     }
  //     return object;
  //   }, {});
  //   const localVue = createLocalVue();
  //     localVue.use(Quasar, { components }); // , lang: langEn
  //     const wrapper = mount(EditarPublicacion, {
  //       localVue,
  //     });

  //     const vm= wrapper.vm;

  //   expect(vm.validateEmail(".correo@mail.com")).toBe(false);
 
  //});
  it('Correos con casos especiales', () => {

    firebase.auth.mockReturnValueOnce({
      currentUser: { email: 'correo.com@mail.com', uid: 1, emailVerified: true }, 
    });
    const components = Object.keys(All).reduce((object, key) => {
      const val = All[key];
      if (val && val.component && val.component.name != null) {
        object[key] = val;
      }
      return object;
    }, {});
    const localVue = createLocalVue();
      localVue.use(Quasar, { components }); // , lang: langEn
      const wrapper = mount(EditarPublicacion, {
        localVue,
      });

      const vm= wrapper.vm;

    expect(vm.validateEmail("correo.com@mail.com")).toBe(true);

  });
  it('URL correcta', () => {
    
    firebase.auth.mockReturnValueOnce({
      currentUser: { email: 'example@gmail.com', uid: 1, emailVerified: true }, 
    });
    const components = Object.keys(All).reduce((object, key) => {
      const val = All[key];
      if (val && val.component && val.component.name != null) {
        object[key] = val;
      }
      return object;
    }, {});
    const localVue = createLocalVue();
      localVue.use(Quasar, { components }); // , lang: langEn
      const wrapper = mount(EditarPublicacion, {
        localVue,
      });

      const vm= wrapper.vm;

    let vtest=vm.validURL("https://www.google.com");
     
    // Assert
    expect(vtest).toBe(true);
  });
  // eslint-disable-next-line jest/no-commented-out-tests
  // it('URL sin protocolo', () => {

  //   firebase.auth.mockReturnValueOnce({
  //     currentUser: { email: 'example@gmail.com', uid: 1, emailVerified: true }, 
  //   });
  //   const components = Object.keys(All).reduce((object, key) => {
  //     const val = All[key];
  //     if (val && val.component && val.component.name != null) {
  //       object[key] = val;
  //     }
  //     return object;
  //   }, {});
  //   const localVue = createLocalVue();
  //     localVue.use(Quasar, { components }); // , lang: langEn
  //     const wrapper = mount(EditarPublicacion, {
  //       localVue,
  //     });

  //     const vm= wrapper.vm;

  //   let vtest=vm.validURL("www.google.com");

  //   expect(vtest).toBe(false); // revisar

  // });
});

