import { mount, createLocalVue, shallowMount } from '@vue/test-utils';
import detalle from '../../../../src/pages/DetallePublicacion.vue';
import * as All from 'quasar';
// import langEn from 'quasar/lang/en-us' // change to any language you wish! => this breaks wallaby :(
const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});


describe('Functional Suitability-Functional Correctness: RF006-Ver detalles de publicación', () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, { components }); // , lang: langEn

  const wrapper = mount(detalle, {
    localVue,
  });
  const vm = wrapper.vm;

  it('Email Correcto', () => {
    // Arrange
    let email_for_testing = "holalola@gmail.com";
     
    // Act
    let test_answer = vm.validateEmail(email_for_testing);
     
    // Assert
    expect(test_answer).toBe(true);


  });
  it('Email incorrecto sin dominio', () => {
    // Arrange
    let email_for_testing = "holalola@";
     
    // Act
    let test_answer = vm.validateEmail(email_for_testing);
     
    // Assert
    expect(test_answer).toBe(false);
  });
  it('Email incorrecto sin @', () => {
    // Arrange
    let email_for_testing = "holalolagmail.com";
     
    // Act
    let test_answer = vm.validateEmail(email_for_testing);
     
    // Assert
    expect(test_answer).toBe(false);
  });
  it('URL correcta', () => {
    // Arrange
    let url_for_testing = "https://hola.com/F3RfP60bz6NSpCFHi66X";

    // Act
    let test_answer = vm.validURL(url_for_testing)
     
    // Assert
    expect(test_answer).toBe(true);
  });
  // eslint-disable-next-line jest/no-commented-out-tests
  // it('URL sin protocolo', () => {
  //   // Arrange
  //   let url_for_testing = "marcopolo.com/home";

  //   // Act
  //   let test_answer = vm.validURL(url_for_testing)
     
  //   // Assert
  //   expect(test_answer).toBe(false);
  // });
  it('URL sin dominio', () => {
   // Arrange
   let url_for_testing = "https://abc";

   // Act
   let test_answer = vm.validURL(url_for_testing)
    
   // Assert
   expect(test_answer).toBe(false);
  
  });
});
