import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import VerPublicaciones from "../../../../src/pages/VerPublicaciones.vue";
import * as All from "quasar";
import { dba } from "../../../../src/boot/firebase";
import firebase from "firebase";
import cons from "consolidate";
// import langEn from 'quasar/lang/en-us' // change to any language you wish! => this breaks wallaby :(
const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components }); // , lang: langEn

const perdidosArray = [
  {
    Correo: "a@a.com",
    Descripcion: "descripcion01",
    Fecha: "1618341498238",
    Foto: "imagen.png",
    id: "id01",
    Nombre: "nombre01",
    Tipo: "Perdido",
    Ubicacion: "Provincia01"
  },
  {
    Correo: "a@a.com",
    Descripcion: "descripcion02",
    Fecha: "1618341498238",
    Foto: "imagen.png",
    id: "id02",
    Nombre: "nombre02",
    Tipo: "Perdido",
    Ubicacion: "Provincia02"
  }
];

const adopcionArray = [
  {
    id: "id01",

    Nombre: "nombre01",
    Descripcion: "descripcion01",
    Provincia: "Provincia01",
    Tipo: "Adopción",
    Foto: "imagen.png",
    Correo: "a@a.com",
    Fecha: "1618341498238"
  },
  {
    id: "id02",
    Nombre: "nombre02",
    Descripcion: "descripcion02",
    Provincia: "Provincia02",
    Tipo: "Adopción",
    Foto: "imagen.png",
    Correo: "a@a.com",
    Fecha: "1618341498238"
  }
];

describe("Functional Suitability-Functional Correctness: RF005-Descubrir publicación", () => {

  it("Conexion no funcional a firebase en perdidos", async () => {
    const fwhere = jest.fn().mockImplementation((a, b, c) => {return undefined;});
  
    dba.collection = jest.fn().mockImplementation(a => {
      return { where: fwhere };
    });

    // Arrange
    let expected = [];

    //assert
    const vm = await vmWrapper();

    expect(vm.perdidos).toStrictEqual(expected);
  });

  it("Si hay conexion a firebase, pero la respuesta no contiene perdidos", async () => {
    mockUp("",1);

    // Arrange
    let expected = [];

    //assert
    const vm = await vmWrapper();

    expect(vm.perdidos).toStrictEqual(expected);
  });
  it("Si hay conexion a firebase, pero la respuesta viene vacia para perdidos", async () => {
    // Arrange
    mockUp("adopcion",0);

    // Arrange
    let expected = [];

    //assert
    const vm = await vmWrapper();
    

    expect(vm.perdidos).toStrictEqual(expected);
  });
  it("revisar que el metodo liste los perdidos en el objeto perdidos", async () => {
    // Arrange
    mockUp("perdido",1);

    // Arrange
    let expected = perdidosArray;

    //assert
    const vm = await vmWrapper();

    expect(vm.perdidos).toStrictEqual(expected);
  });

  it("Conexion no funcional a firebase en adoptados", async () => {
    const fwhere = jest.fn().mockImplementation((a, b, c) => {return undefined;});
  
    dba.collection = jest.fn().mockImplementation(a => {
      return { where: fwhere };
    });

    // Arrange
    let expected = [];

    //assert
    const vm = await vmWrapper();

    expect(vm.adoptados).toStrictEqual(expected);
  });

  it("Si hay conexion a firebase, pero la respuesta no contiene adoptados", async () => {
    // Arrange
    mockUp("",0);

    // Arrange
    let expected = [];

    //assert
    const vm = await vmWrapper();

    expect(vm.adoptados).toStrictEqual(expected);
  });
  it("Si hay conexion a firebase, pero la respuesta viene vacia para adoptados", async () => {
    // Arrange
    mockUp("perdido",1);

    // Arrange
    let expected = [];

    //assert
    const vm = await vmWrapper();

    expect(vm.adoptados).toStrictEqual(expected);
  });
  it("revisar que el metodo liste los perdidos en el objeto adoptados", async () => {
    // Arrange
    mockUp("adopcion",0);

    // Arrange
    let expected = adopcionArray;

    //assert
    const vm = await vmWrapper();

    expect(vm.adoptados).toStrictEqual(expected);
  });
});

async function vmWrapper() {

  const wrapper = mount(VerPublicaciones, {
    localVue
  });
  return await wrapper.vm;
}

function dbaDatos(tipo) {
  if (tipo == "perdido") {
    return [
      {
        id: "id01",
        data: () => {
          return {
            Nombre: "nombre01",
            Descripcion: "descripcion01",
            Ubicacion: "Provincia01",
            Tipo: "Perdido",
            Foto: "imagen.png",
            Correo: "a@a.com",
            FechaPublicacion: "1618341498238"
          };
        }
      },
      {
        id: "id02",
        data: () => {
          return {
            Nombre: "nombre02",
            Descripcion: "descripcion02",
            Ubicacion: "Provincia02",
            Tipo: "Perdido",
            Foto: "imagen.png",
            Correo: "a@a.com",
            FechaPublicacion: "1618341498238"
          };
        }
      }
    ];
  } else if (tipo == "adopcion") {
    return [
      {
        id: "id01",
        data: () => {
          return {
            Nombre: "nombre01",
            Descripcion: "descripcion01",
            Provincia: "Provincia01",
            Tipo: "Adopción",
            Foto: "imagen.png",
            Correo: "a@a.com",
            FechaPublicacion: "1618341498238"
          };
        }
      },
      {
        id: "id02",
        data: () => {
          return {
            Nombre: "nombre02",
            Descripcion: "descripcion02",
            Provincia: "Provincia02",
            Tipo: "Adopción",
            Foto: "imagen.png",
            Correo: "a@a.com",
            FechaPublicacion: "1618341498238"
          };
        }
      }
    ];
  } else {
    return [];
  }
}

function mockUp(tipoDato,ifWhere) {

  const fget = jest.fn().mockImplementation(() => {
    return dbaDatos(tipoDato);
  });
  

  let consWhere=()=>{
    if(ifWhere==1){
      return jest.fn().mockImplementation((a, b, c) => {
        if(c=="Perdido"){
        return { get: fget };}
        else{
          return {get: jest.fn().mockImplementation(() => {return []})};
        }
      });
    }
    else{
        return jest.fn().mockImplementation((a, b, c) => {
          if(c=="Adopción"){
          return { get: fget };}
          else{
            return {get: jest.fn().mockImplementation(() => {return []})};
          }
        });
    }
  };


  const fwhere = consWhere();

  dba.collection = jest.fn().mockImplementation(a => {
    return { where: fwhere };
  });

}