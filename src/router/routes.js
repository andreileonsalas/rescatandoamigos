
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'VerPublicaciones', component: () => import('pages/VerPublicaciones.vue') },
      { path: 'CrearPublicacion', component: () => import('pages/CrearPublicacion.vue') },
      {
        path: 'DetallePublicacion/:postid',
        component: () => import('pages/DetallePublicacion.vue'),
        props: true,
      },
      {
        path: 'EditarPublicacion/:postid',
        component: () => import('pages/EditarPublicacion.vue'),
        props: true,
      },
      {
        path: 'EditarUsuario/:userid',
        component: () => import('pages/EditarUsuario.vue'),
        props: true,
      },
      { path: 'Buscar', component: () => import('pages/Buscar.vue') },

      { path: 'Perfil', component: () => import('pages/PerfilDesdeUsuario.vue') },
      { path: 'Registro', component: () => import('pages/Registro.vue') },
      { path: 'Perfiles', component: () => import('pages/Perfil.vue') },
      { path: 'auth', component: () => import('pages/Login.vue') },
      { path: 'admin', component: () => import('pages/AdminPanel.vue') },
      { path: '/', component: () => import('pages/Perfil.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
