# DetallePublicacion

## Props

<!-- @vuese:DetallePublicacion:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|postid|-|—|`false`|-|

<!-- @vuese:DetallePublicacion:props:end -->


## Methods

<!-- @vuese:DetallePublicacion:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|listarInfo|Busca la informacion del post|No recibe argumentos|

<!-- @vuese:DetallePublicacion:methods:end -->


