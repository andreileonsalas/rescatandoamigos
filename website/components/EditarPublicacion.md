# EditarPublicacion

## Props

<!-- @vuese:EditarPublicacion:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|postid|-|—|`false`|-|

<!-- @vuese:EditarPublicacion:props:end -->


## Methods

<!-- @vuese:EditarPublicacion:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|modificar|Modificar un post en la base de datos|sdfadfsafdsa|
|actualizarInformacion|Actualiza la informacion del post en la base de datos con los nuevos elementos ingresados por el usuario|-|
|listarInfo|Popular la Informacion del Post Muestra la Informacion Del post seleccionado|-|
|gotopage|Genera una plantilla de correo para contactar con la persona que hizo la publicacion|-|
|validateEmail|Valida que el correo electrónico guardado sea válido|-|
|validURL|Verifica que el string ingresado compla con el patron generado|-|
|revisarAdmin|Verifica que el usuario actual tenga la propiedad de administrador|-|
|verificarAdmin|Verifica que el usuario actual cumpla con los requisitros para ejecutar opciones de administrador Redirige al usuario en caso de que no sea administrador|-|

<!-- @vuese:EditarPublicacion:methods:end -->


