# EssentialLink

## Props

<!-- @vuese:EssentialLink:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|title|-|`String`|`true`|-|
|caption|-|`String`|`false`|-|
|link|-|`String`|`false`|#|
|icon|-|`String`|`false`|-|

<!-- @vuese:EssentialLink:props:end -->


